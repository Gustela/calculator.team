﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Calc
    {
        public double sum(double a, double b) { return a + b; }
        public double dif(double a, double b) { return a - b; }
        public double mul(double a, double b) { return a * b; }
        public double div(double a, double b) { return a / b; }
        public double pow(double a, double b)
        {
            double sum = 1;
            for (int i = 0; i < b; i++)
            {
                sum *= a;
            }
            return sum;
        }

    }
}
